import {
  JupyterFrontEnd,
  JupyterFrontEndPlugin
} from '@jupyterlab/application';

/**
 * Initialization data for the demo-extension extension.
 */
const extension: JupyterFrontEndPlugin<void> = {
  id: 'demo-extension',
  autoStart: true,
  activate: (app: JupyterFrontEnd) => {
    console.log('JupyterLab extension demo-extension is activated!');
  }
};

export default extension;
